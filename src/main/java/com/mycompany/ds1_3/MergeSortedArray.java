/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.ds1_3;

/**
 *
 * @author PT
 */
public class MergeSortedArray {

    public void merge(int[] nums1, int m, int[] nums2, int n) {
        int i = m - 1; // length nums1 cuz length start by 0
        int j = n - 1; // length nums2 cuz length start by 0
        int k = m + n - 1; // length merged array cuz length start by 0
        
        while (i >= 0 && j >= 0) {//mean length nums1 >= 0 and length nums2 >= 0
            if (nums1[i] > nums2[j]) {
                nums1[k] = nums1[i];
                i--;
            } else {
                nums1[k] = nums2[j];
                j--;
            }
            k--;
        }
        
        // If there are remaining elements in nums2
        while (j >= 0) {
            nums1[k] = nums2[j];
            j--;
            k--;
        }
    }
    
    public static void main(String[] args) {
        int[] nums1 = {1,2,3,0,0,0};
        int m = 3;
        int[] nums2 = {2,5,6};
        int n = 3;
        
        MergeSortedArray merger = new MergeSortedArray();
        merger.merge(nums1, m, nums2, n); //merge function
        
        // Printing merged array
        for (int num : nums1) {
            System.out.print(num + " ");
        }
    }
}
